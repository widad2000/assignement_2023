package ma.octo.assignement.service;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;


import java.util.Arrays;
import java.util.List;

public interface ITransactionService {

    List<Transfer> loadAllTransfer();
    void createTransfer(TransferDto transferDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException;
    void createDeposit(DepositDto depositDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException;

    List<Deposit> loadAllDeposit();
}
