package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

import java.util.List;

public interface ICompteService {
    List<Compte> loadAllCompte();
    Compte findCompteByNum(TransferDto transferDto);
    Compte findCompteByRib(DepositDto depositDto);
    Compte SaveCompte(Compte compte);
}
