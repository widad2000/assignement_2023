package ma.octo.assignement.service.tools;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;

@Slf4j
public class Validator {
    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;
    public static final int MONTANT_VIDE = 0;
    public static Boolean validateTransfer(Compte emetteur, Compte beneficiaire , TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        if (emetteur == null) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (beneficiaire == null) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (transferDto.getMontant()==null || transferDto.getMontant().compareTo(new BigDecimal(MONTANT_VIDE)) ==0 ) {
            log.error("Montant vide");
            throw new TransactionException("Montant vide");
        }  else if (transferDto.getMontant().compareTo(new BigDecimal(MONTANT_MINIMAL)) ==-1) {
            log.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().compareTo(new BigDecimal(MONTANT_MAXIMAL)) ==1) {
            log.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif()==null || transferDto.getMotif().isBlank()) {
            log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (emetteur.getSolde().subtract(transferDto.getMontant()).compareTo(new BigDecimal(MONTANT_VIDE))==0 ) {
            log.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
        return true;
    }

    public static Boolean validateDeposit(Compte beneficiare, DepositDto depositDto) throws CompteNonExistantException, TransactionException{

        if (beneficiare == null) {
            log.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }


        if (depositDto.getMontant()==null || depositDto.getMontant().compareTo(new BigDecimal(MONTANT_VIDE)) ==0 ) {
            log.error("Montant vide");
            throw new TransactionException("Montant vide");
        }  else if (depositDto.getMontant().compareTo(new BigDecimal(MONTANT_MINIMAL)) ==-1) {
            log.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (depositDto.getMontant().compareTo(new BigDecimal(MONTANT_MAXIMAL)) ==1) {
            log.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (depositDto.getMotif()==null || depositDto.getMotif().isBlank()) {
            log.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        return true;
    }
}
