package ma.octo.assignement.service;

import ma.octo.assignement.domain.util.EventType;

public interface IAuditService {
    void audit(String message, EventType eventType);
}
