package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping( "Accounts")
public class CompteController {

    @Autowired
    private ICompteService compte;

    @GetMapping("listOfAccounts")
    public List<Compte> loadAllCompte() {

        return compte.loadAllCompte();
    }
}
