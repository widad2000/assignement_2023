package ma.octo.assignement.web;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import static java.util.stream.Collectors.toList;
@Slf4j
@RestController
@RequestMapping("/transaction")
class TransactionController {


    @Autowired
    private ICompteService compteService;

    @Autowired
    private ITransactionService transactionService;

    @GetMapping("listTransfers")
    List<TransferDto> loadAllTransfer() {

        log.info("Lister des transferts");
       return transactionService.loadAllTransfer().stream()
               .map(TransferMapper :: map)
               .collect(toList());
    }

    @PostMapping("/executeTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransfert(@RequestBody TransferDto transferDto)
            throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

       transactionService.createTransfer(transferDto);

    }

    @GetMapping("listDeposits")
    List<DepositDto> loadAll() {

        log.info("Lister des transferts");
        return transactionService.loadAllDeposit().stream()
                .map(DepositMapper:: map)
                .collect(toList());
    }
    @PostMapping("/executeDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit (@RequestBody DepositDto depositDto)
            throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {



        transactionService.createDeposit(depositDto);

    }

}
