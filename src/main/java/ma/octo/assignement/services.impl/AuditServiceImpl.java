package ma.octo.assignement.services.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.IAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
@Transactional
public class AuditServiceImpl implements IAuditService {


    @Autowired
    private AuditRepository auditRepository;

    public void audit(String message,EventType eventType) {

        log.info("Audit de l'événement {}", eventType);

        Audit audit = new Audit();
        audit.setEventType(eventType);
        audit.setMessage(message);
        auditRepository.save(audit);
    }



}
