package ma.octo.assignement.services.impl;


import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Builder.DepositBuilder;
import ma.octo.assignement.domain.Builder.TransferBuilder;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.DepositRepository;

import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.ITransactionService;
import ma.octo.assignement.service.tools.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
public class TransactionServiceImpl implements ITransactionService {

    @Autowired
    private TransferRepository transferRepository;
    @Autowired
    private DepositRepository depositRepository;

    @Autowired
    private IAuditService audit;
    @Autowired
    private ICompteService compteService;
    public List<Transfer> loadAllTransfer() {
        return transferRepository.findAll();

    }
    public List<Deposit> loadAllDeposit() {
        return depositRepository.findAll();

    }

    public void createTransfer( TransferDto transferDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        Compte emetteur = compteService.findCompteByNum(transferDto);
        Compte beneficiaire = compteService.findCompteByNum(transferDto);

       if(Validator.validateTransfer(emetteur,beneficiaire,transferDto)){


           emetteur.setSolde(emetteur.getSolde().subtract(transferDto.getMontant()));
           compteService.SaveCompte(emetteur);
           beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
           compteService.SaveCompte(beneficiaire);



           Transfer transfer = new TransferBuilder(emetteur,beneficiaire)
                   .montantTransaction(transferDto.getMontant())
                   .DateExcecution(transferDto.getDate())
                   .build();



           transferRepository.save(transfer);


           audit.audit("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                   .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                   .toString(), EventType.TRANSFER);


       }
    }

    @Override
    public void createDeposit(DepositDto depositDto) throws TransactionException, CompteNonExistantException{

        Compte beneficiaire = compteService.findCompteByRib(depositDto);

        if(Validator.validateDeposit(beneficiaire,depositDto)){

            beneficiaire.setSolde(new BigDecimal(beneficiaire.getSolde().intValue() + depositDto.getMontant().intValue()));
            compteService.SaveCompte(beneficiaire);


            Deposit deposit = new DepositBuilder(beneficiaire)
                    .montantTransaction(depositDto.getMontant())
                    .DateExcecution(depositDto.getDate())
                    .motifDeposit(depositDto.getMotif())
                    .build();


            depositRepository.save(deposit);


            audit.audit("Deposit vers " + depositDto
                    .getRib() + " d'un montant de " + depositDto.getMontant()
                    .toString(), EventType.DEPOSIT);


        }
    }

}
