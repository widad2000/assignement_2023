package ma.octo.assignement.services.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CompteServiceImpl implements ICompteService {
    @Autowired
    private CompteRepository rep1;

    public List<Compte> loadAllCompte() {
        return rep1.findAll();

    }
    public Compte findCompteByNum(TransferDto transferDto){
       return rep1.findByNrCompte(transferDto.getNrCompteEmetteur());
    }
     public Compte findCompteByRib(DepositDto depositDto){
        return rep1.findCompteByRib(depositDto.getRib());
     }
    public Compte SaveCompte(Compte compte){
      return  rep1.save(compte);
    }
}
