
package ma.octo.assignement.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("user").password("user").roles("USER")
                .and()
                .withUser("admin").password("admin").roles("USER", "ADMIN");

    }

    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                //HTTP Basic authentication
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/Accounts/listOfAccounts").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/Users/ListOfUsers").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/transaction/listTransfers").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/transaction/executeTransfers").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/transaction/listDeposits").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/transaction/executeDeposit").hasRole("USER")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }



}
