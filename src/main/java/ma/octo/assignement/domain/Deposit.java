package ma.octo.assignement.domain;


import ma.octo.assignement.domain.Builder.DepositBuilder;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Depo")

public class Deposit extends Transaction {

    public Deposit(){}
    public Deposit(DepositBuilder DepositBuilder) {
        this.montantTransaction = DepositBuilder.montantTransaction;
        this.motifTransaction = DepositBuilder.motifTransaction;
        this.compteBeneficiaire = DepositBuilder.compteBeneficiaire;
        this.dateExecution = DepositBuilder.dateExecution;

    }
}