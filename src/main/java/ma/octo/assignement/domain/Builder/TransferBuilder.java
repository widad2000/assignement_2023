package ma.octo.assignement.domain.Builder;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Transfer;

import java.math.BigDecimal;
import java.util.Date;

public class TransferBuilder extends Transaction {

    public Compte compteEmetteur;
    public TransferBuilder(Compte compteEmetteur , Compte compteBeneficiaire){
        this.compteEmetteur = compteEmetteur;
        this.compteBeneficiaire= compteBeneficiaire;
    }
    public TransferBuilder montantTransaction(BigDecimal montantTransfert){
        this.montantTransaction=montantTransfert;
        return this;
    }
    public TransferBuilder DateExcecution(Date dateExcecution){
        this.dateExecution=dateExcecution;
        return this;
    }
    public TransferBuilder motifTransaction(String motifTransfer){
        this.motifTransaction=motifTransfer;
        return this;
    }
    public Transfer build() {
        Transfer transfer =  new Transfer(this);
        return transfer;
    }
}
