package ma.octo.assignement.domain.Builder;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Deposit;

import java.math.BigDecimal;
import java.util.Date;

public class DepositBuilder extends Transaction {

    public DepositBuilder(Compte compteBeneficiaire){
        this.compteBeneficiaire= compteBeneficiaire;
    }
    public DepositBuilder montantTransaction(BigDecimal montantDepositt){
        this.montantTransaction=montantDepositt;
        return this;
    }
    public DepositBuilder DateExcecution(Date dateExcecution){
        this.dateExecution=dateExcecution;
        return this;
    }
    public DepositBuilder motifDeposit(String motifDeposit){
        this.motifTransaction=motifDeposit;
        return this;
    }
    public Deposit build() {
        Deposit deposit =  new Deposit(this);
        return deposit;
    }
}