package ma.octo.assignement.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
@MappedSuperclass
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    protected BigDecimal montantTransaction;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date dateExecution;


    @ManyToOne
    protected Compte compteBeneficiaire;

    @Column(length = 200)
    protected String motifTransaction;
}
