package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.Builder.TransferBuilder;

import javax.persistence.*;


@Entity
@Table(name = "TRAN")
@Getter
@Setter
public class Transfer extends Transaction{
  @ManyToOne
  private Compte compteEmetteur;


  public Transfer(TransferBuilder transferBuilder){
    this.compteEmetteur=transferBuilder.compteEmetteur;
    this.montantTransaction =transferBuilder.montantTransaction;
    this.motifTransaction=transferBuilder.motifTransaction;
    this.compteBeneficiaire=transferBuilder.compteBeneficiaire;
    this.dateExecution=transferBuilder.dateExecution;


  }


  public Transfer() {

  }
}
