package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;

import ma.octo.assignement.dto.DepositDto;


public class DepositMapper {

    private static DepositDto depositDto;

    public static DepositDto map(Deposit deposit) {
        depositDto = new DepositDto();
        depositDto.setRib(deposit.getCompteBeneficiaire().getRib());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifTransaction());
        depositDto.setMontant(deposit.getMontantTransaction());


        return depositDto;

    }
}
