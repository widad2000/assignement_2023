package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {

    private static TransferDto transferDto;

    public static TransferDto map(Transfer transfer) {
        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransaction());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMontant(transfer.getMontantTransaction());

        return transferDto;

    }
}
