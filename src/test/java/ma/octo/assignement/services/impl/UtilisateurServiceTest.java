package ma.octo.assignement.services.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class UtilisateurServiceTest {

    @Mock
    UtilisateurRepository utilisateurRepository;
    @InjectMocks
    IUtilisateurService utilisateurService = new UtilisateurServiceImpl();

    @Test
    void loadAllUtilisateur() {

        List<Utilisateur> list = new ArrayList<>();
        Utilisateur t1 = new Utilisateur();
        Utilisateur t2 = new Utilisateur();
        Utilisateur t3 = new Utilisateur();

        list.add(t1);
        list.add(t2);
        list.add(t3);

        when(utilisateurRepository.findAll()).thenReturn(list);

        //test
        List<Utilisateur> utilisateursList = utilisateurService.loadAllUtilisateur();

        assertEquals(3, utilisateursList.size());
        verify(utilisateurRepository, times(1)).findAll();

    }
}