package ma.octo.assignement.services.impl;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.DepositRepository;

import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.ITransactionService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest
class TransactionServiceTest {

    @Mock
    TransferRepository transferRepository;
    @Mock
    DepositRepository depositRepository;
    @InjectMocks
    ITransactionService transactionService = new TransactionServiceImpl();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void loadAllTransfer() {
        List<Transfer> list = new ArrayList<>();
        Transfer t1 = new Transfer();
        Transfer t2 = new Transfer();
        Transfer t3 = new Transfer();

        list.add(t1);
        list.add(t2);
        list.add(t3);

        when(transferRepository.findAll()).thenReturn(list);

        //test
        List<Transfer> transfersList = transactionService.loadAllTransfer();

        assertEquals(3, transfersList.size());
        verify(transferRepository, times(1)).findAll();
    }


    @Test
    void createTransfer() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        TransferDto transferDto = new TransferDto();
        ITransactionService myList = mock(ITransactionService.class);
        doNothing().when(myList).createTransfer(isA(TransferDto.class));
        myList.createTransfer(transferDto);

        verify(myList, times(1)).createTransfer(transferDto);
    }

    @Test
    void createDeposit() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {

        DepositDto depositDto = new DepositDto();
        ITransactionService myList = mock(ITransactionService.class);
        doNothing().when(myList).createDeposit(isA(DepositDto.class));
        myList.createDeposit(depositDto);

        verify(myList, times(1)).createDeposit(depositDto);
    }


    @Test
    void loadAllDeposit() {
        List<Deposit> list = new ArrayList<>();
        Deposit d1 = new Deposit();
        Deposit d2 = new Deposit();
        Deposit d3 = new Deposit();

        list.add(d1);
        list.add(d2);
        list.add(d3);

        when(depositRepository.findAll()).thenReturn(list);

        //test
        List<Deposit> depositList = transactionService.loadAllDeposit();

        assertEquals(3, depositList.size());
        verify(depositRepository, times(1)).findAll();
    }
}