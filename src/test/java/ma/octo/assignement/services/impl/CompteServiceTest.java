package ma.octo.assignement.services.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class CompteServiceTest {

    @Mock
    CompteRepository compteRepository;
    @InjectMocks
    ICompteService compteService = new CompteServiceImpl();

    @Test
    void loadAllCompte() {

        List<Compte> list = new ArrayList<>();
        Compte t1 = new Compte();
        Compte t2 = new Compte();
        Compte t3 = new Compte();

        list.add(t1);
        list.add(t2);
        list.add(t3);

        when(compteRepository.findAll()).thenReturn(list);

        //test
        List<Compte> compteList = compteRepository.findAll();

        assertEquals(3, compteList.size());
        verify(compteRepository, times(1)).findAll();



    }

    @Test
    void findCompteByNum() {
        Compte c = new Compte();
        c.setNrCompte("010000A000001000");
        when(compteRepository.findByNrCompte(c.getNrCompte())).thenReturn(c);

        TransferDto transferDto=new TransferDto();
        transferDto.setNrCompteEmetteur("010000A000001000");

        Compte compte = compteService.findCompteByNum(transferDto);

        assertEquals(c.getNrCompte(), compte.getNrCompte());
    }

    @Test
    void findCompteByRib() {
        Compte c = new Compte();
        c.setRib("A123");
        when(compteRepository.findCompteByRib(c.getRib())).thenReturn(c);

        DepositDto depositDto=new DepositDto();
        depositDto.setRib("A123");

        Compte compte = compteService.findCompteByRib(depositDto);

        assertEquals(c.getRib(), compte.getRib());

    }

    @Test
    void saveCompte() {

        Compte c1 = new Compte();
        c1.setNrCompte("12");

        when(compteRepository.save(c1)).thenReturn(c1);

        //test
        Compte compte = compteRepository.save(c1);

        assertEquals(c1.getNrCompte(), compte.getNrCompte());

    }
}