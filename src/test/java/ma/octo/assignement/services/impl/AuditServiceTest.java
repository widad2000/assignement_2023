package ma.octo.assignement.services.impl;

import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.service.IAuditService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@SpringBootTest
class AuditServiceTest {

    String test ="Deposit vers ";
    @Test
    void audit() {

        IAuditService myList = mock(IAuditService.class);
        doNothing().when(myList).audit(isA(String.class), isA(EventType.class));
        myList.audit(test, EventType.TRANSFER);

        verify(myList, times(1)).audit(test, EventType.TRANSFER);
    }
}